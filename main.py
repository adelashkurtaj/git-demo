import re

def get_numbers(file_name):
    my_file = open(file_name, "r")
    content = my_file.read()
    content_list = re.split('[ ,;?!. \n]', content)
    my_file.close()
    my_list= []
    for i in content_list:
        try:
            if int(i):
                my_list.append(int(i))
        except ValueError:
            continue
    return my_list


def find_max(file_name):
    my_list = get_numbers(file_name)
    if my_list == []:
        return "there are no numbers in this file"
    else:
        maxi = my_list[0]
        for j in my_list:
            if j > maxi:
                maxi = j
            else:
                continue
    return maxi


def find_min(file_name):
    my_list = get_numbers(file_name)
    if my_list == []:
        return "there are no numbers in this file"
    else:
        mini = my_list[0]
        for j in my_list:
            if j< mini:
                mini = j
            else:
                continue
        return mini


def find_sum(file_name):
    my_list = get_numbers(file_name)
    if my_list == []:
        return "there are no numbers in this file"
    else:
        sum = 0
        for j in my_list:
            sum += j
        return sum


def find_multi(file_name):
    my_list= get_numbers(file_name)
    if my_list == []:
        return "there are no numbers in this file"
    else: 
        multi = 1
        for j in my_list:
            multi *= j
        return multi

