from dz import find_max, find_min, find_sum, find_multi
import time
import os

def test_max():
    assert find_max("sample.txt") == 123
    assert find_max("none.txt") == "there are no numbers in this file"

def test_min():
    assert find_min("sample.txt") == -2
    assert find_min("none.txt") == "there are no numbers in this file"

def test_sum():
    assert find_sum("sample.txt") == 208
    assert find_sum("none.txt") == "there are no numbers in this file"

def test_multi():
    assert find_multi("sample.txt") == -13284000
    assert find_multi("none.txt") == "there are no numbers in this file"

def test_time():
    start_time = time.time()
    find_max("sample_2.txt")
    find_min("sample_2.txt")
    find_sum("sample_2.txt")
    find_multi("sample_2.txt")
    end_time = time.time()
    assert end_time-start_time <= 1

def test_size():
    size = os.path.getsize("sample.txt")
    assert size/(1024*1024*1024) <= 1
